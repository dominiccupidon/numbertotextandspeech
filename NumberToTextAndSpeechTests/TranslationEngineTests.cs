﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToText.Logic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace NumberToTextTests
{
    [TestClass]
    public class TranslationEngineTests
    {
        private TranslationEngine translationEngine; //Class being tested
        private NumberToTextEngine numberToTextEngine; //Helper variable
        private const long NUM = 837244; //Number being used as input

        /*
         * MODIFIES: this
         * EFFECTS: Initialises the helper variable
         */
        [TestInitialize]
        public void initialiseInput()
        {
            numberToTextEngine = new NumberToTextEngine(NUM);
        }

        /*
         * MODIFIES: this
         * EFFECTS: Tests the translation abilities of the TranslationEngine class
         */
        [TestMethod]
        public void testTranslation()
        {
            //No translation
            translationEngine = new TranslationEngine(numberToTextEngine.getList(), 0);
            List<string> expected = numberToTextEngine.getList();
            List<string> actual = translationEngine.getList();

            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }

            //English to Spanish
            translationEngine = new TranslationEngine(numberToTextEngine.getList(), 1);
            expected = initialiseExpectedSpanishList();
            actual = translationEngine.getList();

            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        /*
         * MODIFIES: this
         * EFFECTS: Tests the string formatting abilities of the TranslationEngine class
         */
        [TestMethod]
        public void testToString()
        {
            //No translation
            translationEngine = new TranslationEngine(numberToTextEngine.getList(), 0);
            Assert.AreEqual(numberToTextEngine.ToString(), translationEngine.ToString());

            //English to Spanish
            translationEngine = new TranslationEngine(numberToTextEngine.getList(), 1);
            Assert.AreEqual(initialiseExpectedSpanishString(), translationEngine.ToString());
        }

        /*
         * MODIFIES: expected
         * EFFECTS: Creates list containing the expected translation of the text in Spanish
         */
        private List<string> initialiseExpectedSpanishList()
        {
            List<string> expected = new List<string>();
            expected.Add("cuatro");
            expected.Add("cuarenta");
            expected.Add("y");
            expected.Add("ciento");
            expected.Add("dos");
            expected.Add("mil");
            expected.Add("seite");
            expected.Add("trienta");
            expected.Add("y");
            expected.Add("ciento");
            expected.Add("ocho");

            return expected;
        }

        /*
         * MODIFIES: expected, translation
         * EFFECTS: Creates string containing the expected translation of the text in Spanish
         */
        private string initialiseExpectedSpanishString()
        {
            List<string> translation = initialiseExpectedSpanishList();
            string expected;

            translation.Reverse();
            expected = String.Join(" ", translation);

            return expected;
        }
    }
}
