﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToText.Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberToTextTests
{
    [TestClass]
    public class TextToNumberEngineTests
    {
        private TextToNumberEngine textToNumberEngine; //Class being tested

        /*
         * MODIFIES: this
         * EFFECTS: Tests to ensure the expected output is given for valid strings
         */
        [TestMethod]
        public void testConversionOnValidString()
        {
            string[] inputs = { 
                "five",
                "thirteen",
                "seventy eight",
                "four hundred and nine",
                "one thousand and thirty four",
                "twelve thousand three hundred and ninety eight",
                "eighty nine thousand seven hundred and two",
                "one hundred and nine thousand five hundred and eighty four",
                "two million three hundred and ten thousand four hundred and fifty",
                "seventeen million four hundred and seventy three",
                "ninety nine million thirty two thousand two hundred and thirteen",
                "six hundred and one million eight hundred and ninety thousand",
                "five billion sixty three million fourty thousand and seventy one",
                "nineteen billion and one hundred",
                "thirty two billion one million six hundred and two thousand and fourty",
                "two hundred and thirteen billion one hundred and ninety five million seven hundred and sixty thousand and ten"
            };
            long[] outputs = { 5, 13, 78, 409, 1034, 12398, 89702, 109584, 
                2310450, 17000473, 99032213, 601890000, 5063040071, 19000000100, 32001602040, 213195760010};

            for (int i = 0; i < inputs.Length; i++)
            {
                try 
                {
                    textToNumberEngine = new TextToNumberEngine(inputs[i]);
                    Assert.AreEqual(outputs[i], textToNumberEngine.getSum());
                }
                catch (Exception)
                {
                    Assert.Fail("String is valid, no exception should have been thrown");
                }
            }
        }

        /*
         * MODIFIES: this
         * EFFECTS: Tests to ensure that invalid strings are properly handled
         */
        [TestMethod]
        public void testConversionOnInvalidString()
        {
            string[] inputs = {
                "fiv",
                "Thirteen",
                "seventy-eight",
                "fourhundred and nine",
                "fifty fog"
            };

            for (int i = 0; i < inputs.Length; i++)
            {
                try
                {
                    textToNumberEngine = new TextToNumberEngine(inputs[i]);
                    Assert.Fail("String is invalid, an exception should have been thrown");
                } 
                catch (Exception e)
                {
                    Assert.AreEqual("Invalid string", e.Message);
                }
            }
        }

        /*
         * MODIFIES: this
         * EFFECTS: Tests to ensure that strings that translate to values above one trillion are not accepted
         */
        [TestMethod]
        public void testConversionOnValuesAboveOneTrillion()
        {
            string[] inputs = { 
                "nineteen thousand and five billion and one hundred", 
                "five hundred and six thousand billion"
            };

            for (int i = 0; i < inputs.Length; i++)
            {
                try
                {
                    textToNumberEngine = new TextToNumberEngine(inputs[i]);
                    Assert.Fail("The number that was requested for is too large; an exception should have been thrown");
                }
                catch (Exception e)
                {
                    Assert.AreEqual("The number that was requested for is too large", e.Message);
                }
            }
        }
    }
}
