﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToText.Logic;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberToTextTests
{
    [TestClass]
    public class NumberToTextEngineTests
    {
        private NumberToTextEngine numberToTextEngine; //Class being tested
        private const long INPUT = 5026489; //Test input value

        /*
         * MODIFIES: this
         * EFFECTS: Initialises necessary fields
         */
        [TestInitialize]
        public void intialiseClass()
        {
            numberToTextEngine = new NumberToTextEngine(INPUT);
        }

        /*
         * MODIFIES: expected, actual
         * EFFECTS: Tests the getList function
         */
        [TestMethod]
        public void testGetList()
        {
            List<string> expected = getExpectedList();
            List<string> actual = numberToTextEngine.getList();

            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        /*
         * EFFECTS: Tests the toString function
         */
        [TestMethod]
        public void testToString()
        {
            Assert.AreEqual(getExpectedString(), numberToTextEngine.ToString());
        }
        
        /*
         * MODIFIES: expected
         * EFFECTS: Creates the expected value that will be tested against the actual result of numberToTextEngine.getList()
         */
        public List<string> getExpectedList()
        {
            List<string> expected = new List<string>();
            expected.Add("nine");
            expected.Add("eighty");
            expected.Add("and");
            expected.Add("hundred");
            expected.Add("four");
            expected.Add("thousand");
            expected.Add("six");
            expected.Add("twenty");
            expected.Add("million");
            expected.Add("five");

            return expected;
        }

        /*
         * MODIFIES: list, expected
         * EFFECTS: Creates the expected value that will be tested against the actual result of numberToTextEngine.toString()
         */
        public string getExpectedString()
        {
            List<string> list = getExpectedList();
            string expected;

            list.Reverse();
            expected = String.Join(" ", list);

            return expected;
        }

    }
}
