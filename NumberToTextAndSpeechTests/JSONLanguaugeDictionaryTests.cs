using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToText.Logic;
using System.IO;
using System.Text.Json;

namespace NumberToTextTests
{
    [TestClass]
    public class JSONLanguaugeDictionaryTests
    {
        private JSONLanguageDictionary languageDictionary; //Class being tested
        private const string FILENAME = @"Resources\JSON\LanguageDictionary.json"; //Location of file needed for initialisation //Issue with getting file

        /*
         * MODIFIES: this
         * EFFECTS: Tests the initialisation process of the class
         */
        [TestInitialize]
        public void testInitialisation()
        {
            string json = File.ReadAllText(Path.GetFullPath(FILENAME));
            languageDictionary = JsonSerializer.Deserialize<JSONLanguageDictionary>(json);
            Assert.AreEqual(33, languageDictionary.getLength());
        }

        /*
         * EFFECTS: Tests to see if the dictionary outputs the correct translations
         */
        [TestMethod]
        public void testGetLanguageTranslation()
        {
            string[] inputs = { "ninety", "seventeen", "and", };
            string[] expected = { "noventa", "dieciseite", "y"};

            for (int i = 0; i < inputs.Length; i++)
            {
                Assert.AreEqual(expected[i], languageDictionary.getTranslation(inputs[i], 1));
            }
        }
    }
}
