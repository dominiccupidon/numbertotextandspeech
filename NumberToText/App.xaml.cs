﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace NumberToText
{
    // Interaction logic for App.xaml
    public partial class App : Application
    {
        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void PART_MINIMIZE_Click(object sender, RoutedEventArgs e)
        {
            App.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        private void PART_MAXIMIZE_RESTORE_Click(object sender, RoutedEventArgs e)
        {
            if (App.Current.MainWindow.WindowState == WindowState.Normal)
            {
                App.Current.MainWindow.WindowState = WindowState.Maximized;
            }
            else
            {
                App.Current.MainWindow.WindowState = WindowState.Normal;
            }
        }

        private void DRAGGABLE(object sender, EventArgs e)
        {
            App.Current.MainWindow.DragMove();
        }
    }
}
