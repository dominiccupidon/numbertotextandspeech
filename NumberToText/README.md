﻿# Number to Text and Speech

## About
Using C#, slight variation of a text-to-speech program was designed.

Users would enter a numerical input e.g. 25 and the corresponding English word and pronunciation would be given
Instead of using an exisiting text-to-speech engine, one was created using my own voice.

The programme also offers the reverse functionality. Therefore the user would enter "sixty five thousand and two hundred and five" and
get 65205

The programme accepts inputs between zero (0) to one trillion (1,000,000,000,000) exclusive. 

## Purpose
Having learned C earlier that year (2017), this programme originally aimed to be an introduction to C# and the idea of Graphical User Interfaces (GUI).
During Christmas of 2020, I decided to revisit the project and apply a more object-oriented approach as against my initial procedural approach, while adding more
features to the programme. 

Overall, this programme serves as my first major personal project.

## Future
Future plans for this project include:

*	Add more translation options along with corresponding voice outputs

*	(Possibility) Add better translation functionality through the use of an API

*	(Considering) Adding support for float values

I am currently aiming to finish these updates before February