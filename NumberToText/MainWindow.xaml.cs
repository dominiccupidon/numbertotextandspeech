﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.IO;
using System.Reflection;

namespace NumberToText
{
    //Logic behind the User Interface
    public partial class MainWindow : Window
    {
        //Range of values available
        private const long MINVAL = 0;
        private const long MAXVAL = 1000000000000;

        private String input; //Input from user
        private int translationCode; //Records the translation option selected by the user
        private Logic.NumberToTextEngine numberToText; //Engine responsible for number to English text conversion
        private Logic.TranslationEngine translation; //Engine responsible for translating English text to another language
        private Logic.TextToNumberEngine textToNumber; //Engine responsible for English text to number conversion

        /*
         * EFFECTS: Initialises the user interface and sets the translationCode to 0
         */
        public MainWindow()
        {
            InitializeComponent();
            translationCode = 0;
        }

        /*
         * MODIFIES: this, data
         * EFFECTS: Accepts and validates user input when button is clicked
         */
        private void submitNumber(object sender, RoutedEventArgs e)
        {
            long data;
            input = InputNum.Text;
            try
            {
                data = long.Parse(input); //Coversion of string to Integer
                InputNum.Clear();
                checkValue(data);
            }
            catch (Exception)
            { //Result if the program cannot do the conversion
                InputNum.Clear();
                OutputText.Content = "Invalid entry";
                OutputText.Foreground = Brushes.Red;
            }
        }

        /*
         * MODIFIES: this
         * EFFECTS: Accepts user input and begins the conversion process
         */
        private void submitText(object sender, RoutedEventArgs e)
        {
            input = InputText.Text;
            try
            {
                textToNumber = new Logic.TextToNumberEngine(input);
                InputText.Clear();
                printNum();
            } catch (Exception ex)
            {
                InputText.Clear();
                OutputNum.Content = ex.Message;
                OutputNum.Foreground = Brushes.Red;
            }
        }

        /*
         * MODIFIES: this, data
         * EFFECTS: Accepts and validates user input when enter key is clicked
         */
        private void enterNumber(object sender, KeyEventArgs e) //Enter Click Method
        {
            long data;
            input = InputNum.Text;
            if (e.Key == Key.Enter)
            {
                try
                {
                    data = long.Parse(input); //Coversion of string to Integer
                    InputNum.Clear();
                    checkValue(data);
                }
                catch (Exception)
                { //Result if the program cannot do the conversion
                    InputNum.Clear();
                    OutputText.Content = "Invalid entry";
                    OutputText.Foreground = Brushes.Red;
                }
            }
        }

        /*
        * MODIFIES: this, data
        * EFFECTS: Accepts and validates user input when enter key is clicked
        */
        private void enterText(object sender, KeyEventArgs e) //Enter Click Method
        {
            input = InputText.Text;
            if (e.Key == Key.Enter)
            {
                try
                {
                    textToNumber = new Logic.TextToNumberEngine(input);
                    InputText.Clear();
                    printNum();
                }
                catch (Exception ex)
                { //Result if the program cannot do the conversion
                    InputText.Clear();
                    OutputNum.Content = ex.Message;
                    OutputNum.Foreground = Brushes.Red;
                }
            }
        }

        /*
         * MODIFIES: this
         * EFFECTS: Gets translation input from user
         */
        private void optionSelected(object sender, SelectionChangedEventArgs e)
        {
            translationCode = (int)(OutputLanguage.SelectedIndex);
        }

        /*
         * MODIFIES: this
         * EFFECTS: Determines if num is an acceptable input. If so begins conversion, otherwise prints error
         */
        private void checkValue(long num) //Ensures input is within a certain range
        {

            if ((num <= MINVAL) || (num >= MAXVAL))
            {
                OutputText.Foreground = Brushes.Red;
                OutputText.Content = "You can only enter a number greater than 0 \nbut less than 1,000,000,000,000";
            }
            else
            {
                numberToText = new Logic.NumberToTextEngine(num);
                translation = new Logic.TranslationEngine(numberToText.getList(), translationCode);
                printText();
            }
        }

        /*
         * MODIFIES: this, output
         * EFFECTS: Prints converted number to the screen
         */
        private void printText() 
        {
            string output;
            if (translationCode == 0) //Determines which language to be outputted
            {
                output = numberToText.ToString();
            } else
            {
                output = translation.ToString();
            }
            OutputText.Foreground = Brushes.Green;
            OutputText.Content = "Input: " + input + "\nResult: " + output;
        }

        /*
         * MODIFIES: this, output
         * EFFECTS: Prints converted text to the screen
         */
        private void printNum()
        {
            long output = textToNumber.getSum();
            OutputNum.Foreground = Brushes.Green;
            OutputNum.Content = "Input: " + input + "\nResult: " + output;
        }

        /*
         * MODIFIES: culture, keyword, file
         * EFFECTS: Creates and plays a sequence of voice clips based on keyword
         */
        private void voiceClick(object sender, RoutedEventArgs e)
        {
            try
            {
                TextInfo culture = new CultureInfo("en-US", false).TextInfo;
                List<string> keyword = numberToText.getList();
                List<string> file = new List<string>(); //List to store the file names of the audio files

                //The assignment below works as the file names are the same as the English words said within in them
                foreach (var word in keyword)
                {
                    file.Add(culture.ToTitleCase(word));
                }
                //Finding and playing each file
                foreach (var name in file)
                {
                        var voice = new System.Media.SoundPlayer(Path.GetFullPath(@"Resources\Audio\" + name + ".wav"));
                        voice.PlaySync();
                        pause();
                }
            }
            catch (Exception)
            {
                errorPrompt();
            }
            reset();
        }

        /*
         * MODIFIES: this
         * EFFECTS: Outputs error to screen if playing audio files fail
         */
        private void errorPrompt()
        {
            IconImage.Source = new BitmapImage(new Uri(@"Resources/Images/icons8-play-16.png", UriKind.Relative));
            Label.Content = "Play";
            Error.Content = "Error in playing";
            Error.Foreground = Brushes.Red;
        }

        /*
         * MODIFIES: this
         * EFFECTS: Resets button icon and label
         */
        private async void reset()
        {
            await Task.Delay(3000);
            IconImage.Source = new BitmapImage(new Uri(@"Resources/Images/icons8-play-16.png", UriKind.Relative));
            Label.Content = "Play";
            Error.Content = " ";
        }

        /*
         * EFFECTS: Places a pause between each audio file
         */
        private async void pause()
        {
            await Task.Delay(500);
        }
    }
}
