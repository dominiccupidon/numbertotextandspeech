﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace NumberToText.Logic
{
    //Responsible for translating the English result
    public class TranslationEngine
    {
        private const string FILENAME = @"Resources\JSON\LanguageDictionary.json"; //Location of JSON file used to populate the dictionary of translations
        private List<string> translation; //Stores the translated phrase

        /*
         * EFFECTS: Initialises translation and begins process of translation
         */
        public TranslationEngine(List<string> text, int code)
        {
            translation = new List<string>();
            createTranslation(text, code);
        }

        /*
         * REQUIRES: text != NULL, translationCode is between 0 and the number of available languages - 1
         * MODFIIES: this, newWord, languageDictionary
         * EFFECTS: Assigns an appropriate value to translation, initialising and using values from languageDictionary if necessary
         */
        private void createTranslation(List<string> text, int translationCode)
        { 
            if (translationCode == 0) //Checks if output language is English
            {
                translation = new List<string>(text);
            } else
            {
                //Pulls the translations from the JSON file
                string json = File.ReadAllText(Path.GetFullPath(FILENAME)); 
                JSONLanguageDictionary languageDictionary = JsonSerializer.Deserialize<JSONLanguageDictionary>(json);

                //Translates each word and adds it to translation
                foreach (string word in text)
                {
                    string newWord = " ";
                    if (!word.Equals(" "))
                    {
                        newWord = languageDictionary.getTranslation(word, translationCode);
                    }
                    translation.Add(newWord);
                }
            }
        }

        /*
         * MODIFIES: this, output
         * EFFECTS: Last updates to translation, before the final output is produced
         */
        public override string ToString()
        {
            translation.Reverse();
            string output = String.Join(" ", translation);
            return output;
        }

        /*
         * EFFECTS: Gives access to translation
         */
        public List<string> getList()
        {
            return translation;
        }
    }
}
