﻿using System;
using System.Collections.Generic;

namespace NumberToText.Logic
{
    //Class responsible for storing list of translations
    public class JSONLanguageDictionary
    {
        public Dictionary<string, string[]> languageDict { get; set; } //Dictionary to match the Enlgish word to its the literal translations

        /*
         * REQUIRES: word to be a key in the dictionary and translationCode is between 1 and the size of languageDict[word], (inclusive)
         * MODIFIES: translations
         * EFFECTS: Using word as the key and translationCode as the index, finds the appropriate translation for an English word
         */
        public string getTranslation(string word, int translationCode)
        {
            string[] translations = languageDict[word];
            return translations[translationCode - 1];
        }

        /*
         * EFFECTS: Gives the number of elements stored in the dictionary
         */
        public int getLength()
        {
            return languageDict.Count;
        }
    }
}
