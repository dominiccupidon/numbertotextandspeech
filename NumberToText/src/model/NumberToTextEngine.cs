﻿using System;
using System.Collections.Generic;

namespace NumberToText.Logic
{
    //Responsible for the conversion of numbers to an English word
    public class NumberToTextEngine
    {
        private int[] digits; //Holds digits of inputted number
        private List<string> keyword; //List to hold text

        /*
         * REQUIRES: 0 <= num <= 1000000000000
         * EFFECTS: Declares digits and keywords. Begins conversion of number to English text
         */
        public NumberToTextEngine(long num)
        {
            digits = new int[11];
            keyword = new List<string>();
            orderDigits(num);
        }

        /*
         * MODIFIES: this, placeholder
         * EFFECTS: Breaks down num into digits and assigns them to digits[i]
         */
        private void orderDigits(long num) 
        {
            long placeholder;
            int i = 0;
            placeholder = num;
            while (placeholder > 0)
            {
                digits[i] = (int)placeholder % 10; //Takes the last digit in the number and places in an array slot
                placeholder = placeholder / 10; //Breaks down number even further
                ++i; //Increment of Counter value
            }
            convertNumToWord(i);
        }

        /*
         * REQUIRES: max > 0
         * MODIFIES: this, indivBehind, indivDigit
         * EFFECTS: Takes the members of digits and determines the right English word to add to keyword
         */
        private void convertNumToWord(int max)
        {
            int indivBehind, indivDigit;

            for (int i = 0; i < max; i++)
            {
                indivDigit = digits[i];
                // Method to populate digits e.g.One Hundred, Two Million
                if ((i != 1) && (i != 4) && (i != 7) && (i != 10))
                {
                    keyword.Add(toWord(indivDigit));
                }
                else
                {
                    //Method used to populate digit places with Ten eg. Ten Thousand and Thirty Million
                    if ((i == 1) || (i == 4) || (i == 7) || (i == 10))
                    {
                        indivBehind = digits[i - 1];
                        keyword.Add(tensToWord(indivDigit, indivBehind));
                        if ((digits[i] == 1) && (digits[i - 1] > 0))
                            keyword[i - 1] = "zero";
                    }
                }
            }
            wordToEnglish();
        }

        /*
         * EFFECTS: Returns a string based on the value of indivDigit
         */
        private string toWord(int indivDigit)
        {
            switch (indivDigit)
            {
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
                case 7: return "seven";
                case 8: return "eight";
                case 9: return "nine";
                default: return "zero";        
            }
        }

        /*
         * EFFECTS: Returns a string based on the value of tens and possibly ones
         */
        private string tensToWord(int tens, int ones) 
        {
            switch (tens)
            {
                case 1: return teens(ones);
                case 2: return "twenty";
                case 3: return "thirty";
                case 4: return "fourty";
                case 5: return "fifty";
                case 6: return "sixty";
                case 7: return "seventy";
                case 8: return "eighty";
                case 9: return "ninety";
                default: return "zero";
            }
        }

        /*
         * EFFECTS: Returns a string based on the value of ones. This is in order to facilitate values between 10 and 20
         */
        private string teens(int ones)
        {
            switch (ones)
            {
                case 1: return "eleven";
                case 2: return "twelve";
                case 3: return "thirteen";
                case 4: return "fourteen";
                case 5: return "fifteen";
                case 6: return "sixteen";
                case 7: return "seventeen";
                case 8: return "eighteen";
                case 9: return "nineteen";
                default: return "ten";
            }
        }

        /*
         * MODIFIES: this, jump, prevJump, suffix
         * EFFECTS: Updates keyword to reflect the proper structure of English language
         */
        private void wordToEnglish()
        {
            int jump = 0; //How many indexes are to be skipped
            int prevJump = 0; //The location of the last visited index
            string suffix;

            for (int i = 0; i < keyword.Count; i++)
            {
                //Replaces placeholder zeros with spaces
                if ((i >= 0) && (keyword[i].Equals("zero") == true))
                {
                    keyword[i] = " ";
                }
                else
                {
                    if ((i > 1) && ((keyword[i].Equals("zero") == false) && (keyword[i].Equals(" ") == false)))
                    {
                        //Determines which suffix and conjuction is to be added e.g. Hundred And
                        if (jump == 0)
                        {
                            suffix = addSuffix(i, i);
                        }
                        else
                        {
                            int orig = i - prevJump; //Determines if current index is in the Thousands, Millions or Billions places
                            suffix = addSuffix(orig, i);
                            jump = 0;
                        }

                        //Inserts suffix to keyword.
                        if (suffix.Equals(" ") == false)
                        {
                            //suffix is broken into its individual parts before being inserted
                            string[] substrings = suffix.Split(' '); 
                            foreach (var substring in substrings)
                            {
                                keyword.Insert(i, substring);
                                ++jump;
                            }
                        }
                        else
                        {
                            keyword.Insert(i, suffix);
                            ++jump;
                        }

                        prevJump += jump;
                        i += jump;
                    }
                }
            }
            //Removes all unneccessary spaces
            keyword.RemoveAll(isSpace);
        }

        /*
         * EFFECTS: Creates a predicate for the List.RemoveALL function
         */
        private static bool isSpace(string element) 
        {
            return element.Contains(" ");
        }

        /*
         * MODIFIES: suffix
         * EFFECTS: Uses count and current to determine the proper conjunction and suffix to combine
         */
        private string addSuffix(int count, int current)  
        {
            switch (count)
            {
                case 2: return addConjunctionTypeA("hundred");
                case 3: return addConjunctionTypeB("thousand");
                case 4: return addConjunctionTypeD(current, "thousand");
                case 5: return addConjuctionTypeE(current, "thousand");
                case 6: return addConjunctionTypeC(current, "million");
                case 7: return addConjunctionTypeD(current, "million");
                case 8: return addConjuctionTypeE(current, "million");
                case 9: return addConjunctionTypeC(current, "billion");
                case 10: return addConjunctionTypeD(current, "billion");
                case 11: return addConjuctionTypeE(current, "billion");
                default: return " ";
            }
        }

        /*
         * REQUIRES: suffix != " "
         * MODIFIES: suffix
         * EFFECTS: Determines if a conjunction is needed before digits in the Ones or Tens places
         */
        private string addConjunctionTypeA(string suffix)
        {
            if ((keyword[1].Equals(" ") == false) || (keyword[0].Equals(" ") == false))
            {
                suffix = suffix + " " + "and";
            }
            return suffix;
        }

        /*
         * REQUIRES: suffix != " "
         * MODIFIES: suffix, newSuffix
         * EFFECTS: Determines if a conjunction is needed for digits under 10,000
         */
        private string addConjunctionTypeB(string suffix) 
        {
            string newSuffix;
            if (keyword[2].Equals(" "))
            {
                newSuffix = addConjunctionTypeA(suffix);
            }               
            else 
            { 
                newSuffix = suffix; 
            }
                
            return newSuffix;
        }

        /*
         * REQUIRES: suffix != " ", digitOrder >= 3
         * MODIFIES: suffix, newSuffix, check
         * EFFECTS: Handles conjunctions for numbers such as: 2,001 and 10,600,023
         */
        private string addConjunctionTypeC(int digitOrder, string suffix)
        {
            string newSuffix;
            bool check = false;

            //Determines if all the members in keyword [2 .. digitOrder - 1] are spaces
            for (int i = (digitOrder - 1); i > 1; i--)
            {
                if (keyword[i].Equals(" "))
                {
                    check = true;
                }
                else
                {
                    check = false;
                    break;
                }
            }

            //Updates the suffix based on this check
            if (check == true)
            {
                //If check is true: Two Million One becomes Two Million And One
                newSuffix = addConjunctionTypeA(suffix);
            }
            else
            {
                newSuffix = suffix;
            }

            return newSuffix;
        }

        /*
         * REQUIRES: suffix != " ", digitOrder = 3 * i + 1, i > 0
         * MODIFIES: suffix, newSuffix
         * EFFECTS: Handles conjunctions for digits in the Ten Thousands, Ten Millions and Ten Billions places
         */
        private string addConjunctionTypeD(int digitOrder, string suffix)
        {
            string newsuffix;

            /*  
             *  Checks to see if every member after keyword[digitOrder - 1] is a space and chooses the right conjunction
             *  based on that check
            */
            if (keyword[digitOrder - 1].Equals(" ") == false)
            {
                newsuffix = " ";
            }
            else
            {
                newsuffix = addConjunctionTypeC(digitOrder, suffix);
            }

            return newsuffix;
        }

        /*
         * REQUIRES: secondSuffix != " ", digitOrder = 3 * i - 1, i > 1
         * MODIFIES: suffix, secondSuffix
         * EFFECTS: Handles conjunctions for digits in the Hundred Thousands, Hundred Millions and Hundred Billions places
         */
        private string addConjuctionTypeE(int digitOrder, string secondSuffix) 
        {
            string suffix = "hundred";

            //Determines if there are digits in Ones and Tens places and chooses the appropriate conjunction
            if (((keyword[digitOrder - 1].Equals(" ") == false) || ((keyword[digitOrder - 2].Equals(" ") == false))))
            {
                suffix = suffix + " " + "and";
            }
            else
            {
                secondSuffix = addConjunctionTypeC(digitOrder, secondSuffix);
                suffix = suffix + " " + secondSuffix;
            }
            return suffix;
        }

        /*
         * MODIFIES: this, output
         * EFFECTS: Last updates to keyword, before the final output is produced
         */
        public override string ToString() 
        {
            keyword.Reverse();
            string output = String.Join(" ", keyword);
            return output;
        }

        /*
         * EFFECTS: Gives access to keyword
         */
        public List<string> getList()
        {
            return keyword;
        }
    }
}
