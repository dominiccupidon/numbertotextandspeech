﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberToText.Logic
{
    //Responsible for converting English words into numeric values;
    public class TextToNumberEngine
    {
        private List<string> keywords; //Holds the individual words that make up the entire string
        private long sum; //Holds the numerical value of the string 

        /*
         * REQUIRES: word != NULL
         * EFFECTS: Initialises sum and keywords and then begins the conversion
         */
        public TextToNumberEngine(string word)
        {
            keywords = new List<string>();
            sum = 0;
            getValueOfString(word);
        }

        /*
         * REQUIRES: word != NULL
         * MODIFIES: this and word
         * EFFECTS: Determines the sum of the values contained within given a string, e.g. "fifty five" => 55 and "Cat" => -1
         */
        private void getValueOfString(string word)
        {
            string [] words = word.Split(' '); //Splts string into substrings using the " " character as the divider
            //Populating the list
            for (int i = 0; i < words.Length; i++)
            {
                keywords.Add(words[i]);
            }
            keywords.RemoveAll(isAnd); //Removes all instances of "and" in the string
            keywords.Reverse();

            //Process to calculate the value of the inputted string
            long multiplier = 1;
            for (int i = 0; i < keywords.Count; i++)
            {
                i = summing(i, multiplier);
                multiplier = getValueOfIndivStringD(keywords[i]);
            }
        }

        /*
         * REQUIRES: word != NULL
         * MODIFIES:
         * EFFECTS: Returns the appropriate numeric value for an inputted string, e.g. "five" => 5 and "fog" => -1
         */
        private long getValueOfIndivStringA(string word)
        {
            switch (word)
            {
                case "zero": return 0;
                case "one": return 1;
                case "two": return 2;
                case "three": return 3;
                case "four": return 4;
                case "five": return 5;
                case "six": return 6;
                case "seven": return 7;
                case "eight": return 8;
                case "nine": return 9;
                default: return getValueOfIndivStringB(word);
            }
        }

        /*
         * REQUIRES: word != NULL
         * MODIFIES:
         * EFFECTS: Returns the appropriate numeric value for an inputted string, e.g. "fifteen" => 15 and "fog" => -1
         */
        private long getValueOfIndivStringB(string word)
        {
            switch (word)
            {
                case "eleven": return 11;
                case "twelve": return 12;
                case "thirteen": return 13;
                case "fourteen": return 14;
                case "fiveteen": return 15;
                case "sixteen": return 16;
                case "seventeen": return 17;
                case "eighteen": return 18;
                case "nineteen": return 19;
                default: return getValueOfIndivStringC(word);
            }
        }

        /*
         * REQUIRES: word != NULL
         * MODIFIES:
         * EFFECTS: Returns the appropriate numeric value for an inputted string, e.g. "fifty" => 50 and "fog" => -1
         */
        private long getValueOfIndivStringC(string word)
        {
            switch (word)
            {
                case "ten": return 10;
                case "twenty": return 20;
                case "thirty": return 30;
                case "fourty": return 40;
                case "fifty": return 50;
                case "sixty": return 60;
                case "seventy": return 70;
                case "eighty": return 80;
                case "ninety": return 90;
                default: return -1;
            }
        }

        /*
         * REQUIRES: word != NULL
         * MODIFIES:
         * EFFECTS: Returns the appropriate numeric value for an inputted string, e.g. "thousand" => 1000 and "fog" => -1
         */
        private long getValueOfIndivStringD(string word)
        {
            switch (word)
            {
                case "hundred": return 100;
                case "thousand": return 1000;
                case "million": return 1000000;
                case "billion": return 1000000000;
                default: return -1;
            }
        }

        /*
         * EFFECTS: Creates a predicate for the List.RemoveALL function
         */
        private static bool isAnd(string element)
        {
            return element.Equals("and");
        }

        /*
         * REQUIRES: element != NULL
         * EFFECTS: Determines if element is either equal to "hundred", "thousand", "million" or "billion"
         */
        private bool isKeyword(string element)
        {
            switch (element)
            {
                case "hundred": return true;
                case "thousand": return true;
                case "million": return true;
                case "billion": return true;
                default: return false;
            }
        }

        /*
         * REQUIRES: 0 <= index <= keywords.Count and multiplier to be a power of ten that is >= 1;
         * MODIFIES: this, index, multiplier,  value
         * EFFECTS: Calculates the value of sum by breaking down the problem into the following sections:
         *              Less than a thousand
         *              Between a thousand and a million
         *              Between a million and a billion and
         *              Between a billion and a trillion
         *          Throws an exception if an invalid string is in keywords
         */
        private int summing(int index, long multiplier)
        {
            /*
             * The sections above can be further broken down into various subsections, 
             * e.g. The section between a thousand and a million is split at the hundred thousand marker.
             * The loop below adds the values in a subsection to the overall sum
            */
            while (index < keywords.Count) //Checks to see if the end of the list has been reached. Prevents an IndexOutOfRangeException
            {
                if (!isKeyword(keywords[index]))
                {
                    long value = getValueOfIndivStringA(keywords[index]);
                    //Check for invalid strings
                    if (value != -1)
                    {
                        sum += (value * multiplier);
                    }
                    else
                    {
                        throw new Exception("Invalid string");
                    }
                    ++index;
                } else
                {
                    break;
                }

            }

            if (index < keywords.Count) //Checks to see if the end of the list has been reached
            {
                long newMultiplier = getValueOfIndivStringD(keywords[index]); //Determines the value of the new multiplier
                //Check for invalid strings
                if (newMultiplier == -1)
                {
                    throw new Exception("Invalid string");
                }

                if (multiplier > newMultiplier)
                {
                    if (multiplier * newMultiplier >= 1000000000000)
                    {
                        throw new Exception("The number that was requested for is too large");
                    } else
                    {
                        //Means that the function has reached a new subdivision
                        return summing(index + 1, multiplier * newMultiplier);
                    }
                }
                else
                {
                    //Means that the function has reached a new section
                    return index;
                }
            } else
            {
                return index - 1;
            }
        }

        /*
         * EFFECTS: Returns the value of sum
         */
        public long getSum()
        {
            return sum;
        }
    }
}
