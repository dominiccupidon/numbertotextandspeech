﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberToText.UI
{
    //Responsible for populating the ComboBox Options
    class ComboBoxOptions : ObservableCollection<string>
    {
        /*
         * EFFECTS: Adds the languages to the list of options
         */
        public ComboBoxOptions()
        {
            Add("English");
            Add("Spanish");
        }
    }
}
